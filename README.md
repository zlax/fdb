fdb - Forgers database
======
This is the database of forgers saved in CSV format and displayed via javascript on a web page.

Any webserver can be used. To activate the add and edit feature, configure the webserver to use the cgi bash script.

Image files are stored here: https://archive.org/details/forgers