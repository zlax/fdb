#!/bin/bash
printf "Content-type: text/plain\n\n"
if [ "$REQUEST_METHOD" = "POST" ]; then
    if [ "$CONTENT_LENGTH" -gt 0 ]; then
        if [ "$CONTENT_LENGTH" -lt 131072 ]; then
            read -n $CONTENT_LENGTH POST_DATA <&0
            echo "${POST_DATA//!custom_newline!/$'\n'}" >> ../html/fdb/upload.csv
        fi
    fi
fi
exit